# Why this project?
This Automation project for downloading and opening application from google play store 

# Prerequisites to use the project
* JDK
* SDK
* Appium
* IntelliJ IDEA

# Framework and tools
* Appium(http://appium.io/)
* TestNG
* Project: Maven

# Video
[![](http://img.youtube.com/vi/bRHuUqtysxM/0.jpg)](http://www.youtube.com/watch?v=bRHuUqtysxM "")

