import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

public class DownloadAndOpenAppFromPlayStore {
    WebDriver driver;

    @BeforeTest
    public void setUpForAndroid() throws Exception{
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("appPackage", "com.android.vending");
        dc.setCapability("appActivity", "com.android.vending.AssetBrowserActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
    }

    @Test
    public void downloadTheApp() throws Exception {
        System.out.println("Script started");
        By searchField = By.xpath("\t\n" +
                "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView");
        By searchField2 = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.EditText");
        By installBtn = By.xpath("//*[@text='Install']");
        By openBtn = By.xpath("//*[@text='Open']");
        String appName = "facebook lite";


        waitForVisibilityOf(searchField);
        driver.findElement(searchField).click();
        driver.findElement(searchField2).click();
        driver.findElement(searchField2).sendKeys(appName);

        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));

        waitForVisibilityOf(installBtn);
        System.out.println("Search result found");
        driver.findElement(installBtn).click();

        System.out.println("Waiting for the installation complete");
        waitForVisibilityOf(openBtn);
        System.out.println("Installation completed");

        driver.findElement(openBtn).click();
    }

    @AfterTest
    public void endTest(){
        System.out.println("Script completed");
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 400);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
